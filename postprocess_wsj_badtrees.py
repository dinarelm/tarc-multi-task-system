
# Input: badtrees and corresponding word sequences

import os
import sys
import argparse
from nltk.tree import Tree

import postprocess_wsj_trees as wt

def main():

    parser = argparse.ArgumentParser(
        description='Post-process trees of WSJ format for reconstructing malformed trees output by the system' 
    )
    parser.add_argument('hyp', help='File containing the bad hypotheses trees (i.e. malformed trees not parsed with postprocess_wsj_trees.py and manually corrected)')
    parser.add_argument('sources', help='File containing word sequences corresponding to bad trees')  
    args = parser.parse_args()

    treestr = wt.read_sequences( args.hyp )
    sources = wt.read_sequences( args.sources )

    hyptrees = []
    for s in treestr:
        hyptrees.append( Tree.fromstring(s) ) 

    for idx, t in enumerate(hyptrees):
        tree, rem = wt.add_lexical_level(t, sources[idx].split() )
        if len(rem) > 0:
            sys.stderr.write(' Malformed hypothesis tree {}, no way to add the following words: {}\n'.format(hyptrees[idx], rem))
            sys.stdout.flush()
        hyptrees[idx] = tree

    wt.save_sequences( args.hyp + '.lex', [wt.tree_to_string(t) for t in hyptrees] )

if __name__ == '__main__':
    main()
