#!/bin/bash

source ${HOME}/work/tools/venv_python3.7.2_torch1.4_decore0/bin/activate
FAIRSEQ_PATH=${HOME}/work/tools/venv_python3.7.2_torch1.4_decore0/bin/
#PYTHONPATH=${HOME}/work/tools/fairseq/

export RNNTAGGERPATH=${HOME}/work/tools/Seq2Biseq_End2EndSLU/

CORPUS='MADARblock1-complete+TArCLemmatization-FinalStep.Az-Cl-Ar-Tk-Pos-Lm.Ex'
DATA_FORMAT='tabular'
#DATA_PATH=/home/getalp/dinarelm/work/data/Arabish/UltimiDati/TArCLemmatization/Step6/MADAR-Block1+TArC-Block1-6-WithLemma.tab.Latest
DATA_PATH=/home/getalp/dinarelm/work/data/Arabish/UltimiDati/TArCLemmatization/FinalStep/MADAR-Block1+TArC-FullbyGenre-WithLemma.Az-Cl-Ar-Tk-PoS-Lm.Ex.tab
SERIALIZED_DATA=./system_inputs/${CORPUS}.multi-task.data.${DATA_FORMAT}

LR=0.0005
LR_SHRINK=0.96
DROPOUT=0.4	# Best with LSTM: 0.4, best with Transformer 0.1
CLIP_NORM=5.0
WDECAY=0.0001
BATCH=5
MAX_EPOCHS=150
encoder_embed=$((256))
decoder_embed=${encoder_embed}
encoder_size=${encoder_embed}
decoder_size=${encoder_embed}

ENC_LAYERS=2
ATT_HEADS=4
ENC_FFN_DIM=$((2*$encoder_size))
DEC_LAYERS=${ENC_LAYERS}

MODEL_TYPE='lstm' # lstm or transformer
SUBTASK='tarc-lemma'
GRN_OPTIONS='--char-sequences'	# --token-sequences or --char-sequences, or both
CRITERION='tarc_multitask_loss'

ANNEAL=1
WU_EPOCHS=0	# Best with LSTM: 0, best with Transformer: 10 # NOTE: pay attention to the UPDATES_PER_EPOCH however !
if [[ ${MODEL_TYPE} == 'transformer' ]]; then
	WU_EPOCHS=10
fi
UPDATES_PER_EPOCH=1067 # MADAR+Tarc with batch 5: 1067; TArC lemmatization step3, bacth 5: 973; TArC lemmatization step4, batch 5: 1040; TArC lemmatization step5, batch 5: 1108
WU_UPDATES=$((${WU_EPOCHS}*${UPDATES_PER_EPOCH}))
WARMUP_OPTION="" #"--warmup-updates ${WU_UPDATES}"	# Best with LSTM: no warmup
DIM_OPTIONS="--encoder-embed-dim ${encoder_embed} --encoder-hidden-dim ${encoder_size} --decoder-embed-dim ${decoder_embed} --decoder-hidden-dim ${decoder_size} --decoder-out-embed-dim ${decoder_embed}"
REGULARIZATION_OPTIONS="--clip-norm ${CLIP_NORM} --weight-decay ${WDECAY}"	# Best with Transformer: no regularization except of Dropout
if [[ ${MODEL_TYPE} == "transformer" ]]; then
	DIM_OPTIONS="--encoder-embed-dim ${encoder_size} --decoder-embed-dim ${decoder_size}"
	REGULARIZATION_OPTIONS=""
	WARMUP_OPTION="--warmup-updates ${WU_UPDATES}"
fi

LM_OPTIONS="" #"--input-lm MADAR-TArC-CharLM-2sent-shift1-tabular_BATCH5_3xH256-FFNN512-4Heads_LR0.0005_Shr0.96_Dec0.0001_Drop0.5_transformer_WU10Ep_DefaultInit_TEST/checkpoint_best.pt --lm-data system_inputs/MADAR-TArC-CharLM-2sent-shift1.multi-task.data.tabular"
TRANS_OPTIONS="" #"--use-transformer-layers --load-transformer-layers TArCLemmatization-Step4-New-tabular_BATCH5_3xH256-FFNN512-4Heads_LR0.0005_Shr0.96_Dec0.0001_Drop0.1_transformer_WU10Ep_TEST/checkpoint.best5_avg.pt --freeze-transformer-layers"
SAVE_PATH_PREF=${CORPUS}-${DATA_FORMAT}_BATCH${BATCH}_${ENC_LAYERS}xH${encoder_embed}-FFNN${ENC_FFN_DIM}-${ATT_HEADS}Heads_LR${LR}_Shr${LR_SHRINK}_Dec${WDECAY}_Drop${DROPOUT}_${MODEL_TYPE}_WU${WU_EPOCHS}Ep_NoMaskedDecoders_NewPreprocess_DataSplits_TEST/
#CUDA_VISIBLE_DEVICES=1
PYTHONPATH=${HOME}/work/tools/fairseq/ ${FAIRSEQ_PATH}/fairseq-train ${DATA_PATH} ${LM_OPTIONS} ${TRANS_OPTIONS} \
	--task tarc_multitask --arch tarc_multitask_arch --sub-task ${SUBTASK} --model-type ${MODEL_TYPE} --criterion ${CRITERION} --num-workers=0 --distributed-world-size 1 ${GRN_OPTIONS} \
	--pad-reference --save-dir ${SAVE_PATH_PREF} --serialized-data ${SERIALIZED_DATA} --data-format ${DATA_FORMAT} --patience 20 --no-epoch-checkpoints \
	--encoder-normalize-before --encoder-layers ${ENC_LAYERS} --encoder-attention-heads ${ATT_HEADS} --encoder-ffn-embed-dim ${ENC_FFN_DIM} \
	--share-all-embeddings --activation-dropout ${DROPOUT} --attention-dropout ${DROPOUT} \
	--decoder-normalize-before --decoder-layers ${DEC_LAYERS} ${DIM_OPTIONS} \
	--dropout ${DROPOUT} --decoder-attention-heads ${ATT_HEADS} \
	--optimizer adam --lr ${LR} --force-anneal ${ANNEAL} --lr-shrink ${LR_SHRINK} ${WARMUP_OPTION} ${REGULARIZATION_OPTIONS} \
	--max-sentences ${BATCH} --max-epoch ${MAX_EPOCHS} --keep-best-checkpoints 5

