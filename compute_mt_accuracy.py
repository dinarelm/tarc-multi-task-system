
import os
import sys

sys.path.append( '/home/getalp/dinarelm/work/tools/fairseq/fairseq/' )

import torch
import argparse
from tarc_utils import *
from typing import List

pos_fillers = ['emotag', 'foreign']

parser = argparse.ArgumentParser(description='Compute accuracy for the different level of annotations predicted by the Tarc Multi-Task system')
parser.add_argument('reference', help='Reference file')
parser.add_argument('hypothesis', help='Prediction file')
parser.add_argument('--sequence-separator', type=str, default='_SEQ_SEP_', help='Symbol used to separate outputs of different level. This must match the symbol used in the training and test data')
parser.add_argument('--post-processing', type=str, default='none', help='Post-process predictions assuming TIGER pre-processing was applied to the data. 1) none, 2) tiger, 3) tarc')
parser.add_argument('--hypotheses-file', type=str, default='hypotheses.tab', help='File name for saving the generated hypotheses')
parser.add_argument('--save-hypotheses-format', type=str, default='tabular', help='File format for the generated hypotheses. 1) tabular (default) 2) parallel')
parser.add_argument('--token-sequences', action='store_true', default=False, help='Treat sequences as token sequences, i.e. token reconstruction from characters/components is not performed')
args = parser.parse_args()

def tsr_edit_distance(ref, hyp):
    
    ins_weight = 1.0
    del_weight = 1.0
    sub_weight = 1.0
    
    curr_x_size = ref.size(0)
    curr_y_size = hyp.size(0)
    tsr_ed_matrix = torch.FloatTensor(curr_x_size+1, curr_y_size+1).fill_(0)
    for i in range(1, curr_x_size+1):
        tsr_ed_matrix[i,0] = float(i)
    for i in range(1, curr_y_size+1):
        tsr_ed_matrix[0,i] = float(i)

    for ij in range(curr_x_size*curr_y_size):
        i = (ij // curr_y_size)+1
        j = (ij % curr_y_size)+1
        
        tmp_weight = 0
        if ref[i-1] != hyp[j-1]:
            tmp_weight = sub_weight
        tsr_ed_matrix[i,j] = min(tsr_ed_matrix[i-1,j] + del_weight, tsr_ed_matrix[i,j-1] + ins_weight, tsr_ed_matrix[i-1,j-1] + tmp_weight)

    # Back-tracking for error rate computation
    n_ins = 0
    n_del = 0
    n_sub = 0
    back_track_i = curr_x_size
    back_track_j = curr_y_size
    while back_track_i > 0 and back_track_j > 0:
        
        i = back_track_i
        j = back_track_j
        tmp_weight = 0
        if tsr_ed_matrix[i-1,j-1] != tsr_ed_matrix[i,j]:
            tmp_weight = sub_weight
    
        if tsr_ed_matrix[i-1,j] < tsr_ed_matrix[i,j-1]:
            if tsr_ed_matrix[i-1,j] < tsr_ed_matrix[i-1,j-1]:
                n_del += 1
                back_track_i -= 1
            else:
                back_track_i -= 1
                back_track_j -= 1
                if tmp_weight > 0:
                    n_sub += 1
        else:   # tsr_ed_matrix[i-1,j] >= tsr_ed_matrix[i,j-1]
            if tsr_ed_matrix[i,j-1] < tsr_ed_matrix[i-1,j-1]:
                n_ins += 1
                back_track_j -= 1
            else:
                back_track_i -= 1
                back_track_j -= 1
                if tmp_weight > 0:
                    n_sub += 1

    while back_track_i > 0:
        back_track_i -= 1
        n_del += 1
    
    while back_track_j > 0:
        back_track_j -= 1
        n_ins += 1
 
    return (n_ins, n_del, n_sub, curr_x_size)

def compute_wer_from_tensors(ref_lst, hyp_lst, tok_dict):

    ref_idx_lst = []
    hyp_idx_lst = []
    for t in ref_lst:
        if not t in tok_dict:
            tok_dict[t] = len(tok_dict)
        ref_idx_lst.append( tok_dict[t] )
    for t in hyp_lst:
        if not t in tok_dict:
            tok_dict[t] = len(tok_dict)
        hyp_idx_lst.append( tok_dict[t] )

    ref_tsr = torch.LongTensor( ref_idx_lst )
    hyp_tsr = torch.LongTensor( hyp_idx_lst )
    return tsr_edit_distance(ref_tsr, hyp_tsr)

postproc_types = ['none', 'tiger', 'tiger-ext', 'tarc', 'tarc-ext', 'tarc-ext-npos', 'tarc-full', 'madar-trs', 'madar-trs-ex', 'madar-trs-full', 'madar-trs-full-ex', 'tarc-substep1', 'tarc-substep2', 'tarc-substep3', 'tarc-full-npos', 'tarc-lemma', 'tarc-lemma-full']
if not args.post_processing in postproc_types:
    raise ValueError(' Unsupported post-processing type {} (valid types: {})'.format(args.post_processing, postproc_types))

def reconstruct_token(tok_seq, char_flag):
    d_tokens = detect_tokens(tok_seq, char_flag, fillers) 
    if isinstance(d_tokens[0], List):
        for i, l in enumerate(d_tokens):
            d_tokens[i] = ''.join(l)
        d_tokens = [t.lower() for t in d_tokens]
    return d_tokens

def nop(tok, char_flag):

    return tok.split()

def clean_pos_components(tag_components):

    for i in range(len(tag_components)):
        c = tag_components[i]
        if c[0] == '@':
            tag_components[i] = tag_components[i][1:] 
    return ''.join(tag_components).replace('++', '+')

def detect_tokens(token_seq, char_flag, fills):

    res_tokens = []
    sub_tokens = token_seq.split()
    start_idx = -1
    end_idx = 0 
    prev_end = -1

    for i in range(len(sub_tokens)):
        if char_flag:
            if sub_tokens[i] == start_token and start_idx == -1:
                start_idx = i
            elif sub_tokens[i] == start_token:
                assert start_idx != -1
                if i - start_idx > 1:   # Did the model forget a end token marker ?
                    res_tokens.append( sub_tokens[start_idx+1:i] )
                    start_idx = -1
                    prev_end = i

            if sub_tokens[i] == end_token:
                end_idx = i 
                if start_idx != -1:
                    res_tokens.append( sub_tokens[start_idx+1:end_idx] )
                    start_idx = -1
                    prev_end = end_idx
                else:   # Did the model forget a start token marker ?
                    back_idx = i-1
                    while back_idx > prev_end and sub_tokens[back_idx] not in [start_token, end_token]:
                        back_idx -= 1
                    if back_idx <= prev_end:
                        sys.stderr.write(' detect_tokens WARNING: end-token marker not matched by any start-token marker, assuming a start-token marker miss {} \n'.format(sub_tokens[prev_end:end_idx+1]))
                        sys.stderr.flush()
                        res_tokens.append( sub_tokens[prev_end+1:end_idx] )
                        start_idx = -1
                        prev_end = end_idx

                #elif i > 0 and sub_tokens[i-1] != start_token and sub_tokens[i-1] != end_token:
                #    raise ValueError( ' end-token index is not matched by a start-token index' )
            if sub_tokens[i] in fills and start_idx == -1: 
                res_tokens.append( [sub_tokens[i]] )
        else:
            if sub_tokens[i] not in [start_token, end_token]:
                res_tokens.append( [sub_tokens[i]] )
    if char_flag:
        if start_idx != -1 and end_idx < start_idx: 
            res_tokens.append( sub_tokens[start_idx+1:] )
    if len(res_tokens) == 0:
        res_tokens = sub_tokens

    if len(res_tokens) == 0:
        return ['_EMPTY_']

    #res_tokens = [t.lower() for t in res_tokens]

    return res_tokens

def reconstruct_pos(tag_seq, char_flag):
    d_tokens = detect_tokens(tag_seq, char_flag, pos_fillers)

    if isinstance(d_tokens[0], List):
        for i, l in enumerate(d_tokens):
            d_tokens[i] = clean_pos_components( l )
    return d_tokens 

def reconstruct_tiger_labels(tag_seq, char_flag):

    d_tokens = detect_tokens(tag_seq, char_flag, [])
    if isinstance(d_tokens[0], List):
        for i, l in enumerate(d_tokens):
            d_tokens[i] = '.'.join(l)
    return d_tokens

def save_hypotheses(filename, hypotheses, args):

    f = open(filename, 'w', encoding='utf-8')
    for h in hypotheses:
        if args.save_hypotheses_format == 'tabular':
            f.write( '\n'.join(h) + '\n' )
        elif args.save_hypotheses_format == 'parallel':
            f.write( ' '.join(h) + '\n' )
        else:
            raise ValueError( 'Hypotheses output format {} not supported'.format(args.save_hypotheses_format) )
    f.close()

f = open(args.reference, encoding='utf-8')
ref_data = f.readlines()
f.close()

f = open(args.hypothesis, encoding='utf-8')
hyp_data = f.readlines()
f.close()

assert len(ref_data) == len(hyp_data)

print(' - compute_mt_accuracy: read {} sequences'.format(len(ref_data)))

sequence_separator = args.sequence_separator
num_of_tasks = len( ref_data[0].rstrip(' \r\n').split(sequence_separator) )

print(' - compute_mt_accuracy: detected {} tasks'.format(num_of_tasks))
sys.stdout.flush()

if args.post_processing == 'none':
    task_processing = [nop for i in range(num_of_tasks)]
elif args.post_processing == 'tiger':
    task_processing = [nop for i in range(num_of_tasks)]
elif args.post_processing == 'tiger-ext':
    task_processing = (nop, reconstruct_tiger_labels)
elif args.post_processing == 'tarc':
    task_processing = (reconstruct_token, reconstruct_pos)
elif args.post_processing == 'tarc-ext':
    task_processing = (nop, reconstruct_token, reconstruct_pos)
elif args.post_processing == 'tarc-ext-npos':
    task_processing = (nop, reconstruct_token, nop, reconstruct_pos)
elif args.post_processing == 'tarc-full':
    task_processing = (nop, reconstruct_token, reconstruct_token, reconstruct_pos)
elif args.post_processing == 'tarc-full-npos':
    task_processing = (nop, nop, reconstruct_token, reconstruct_token, reconstruct_pos)
elif args.post_processing == 'tarc-lemma':
    task_processing = (nop, reconstruct_token, reconstruct_token, reconstruct_pos, reconstruct_token)
elif args.post_processing == 'tarc-lemma-full':
    task_processing = (nop, reconstruct_token, reconstruct_token, reconstruct_token, reconstruct_pos)
elif args.post_processing == 'madar-trs':
    task_processing = (nop, reconstruct_token)
elif args.post_processing == 'madar-trs-ex':
    task_processing = (nop, reconstruct_pos, reconstruct_token)
elif args.post_processing == 'madar-trs-full':
    task_processing = (nop, reconstruct_token, reconstruct_pos, reconstruct_token)
elif args.post_processing == 'madar-trs-full-ex':
    task_processing = (nop, reconstruct_token, reconstruct_token, reconstruct_pos)
elif args.post_processing == 'tarc-substep1':
    task_processing = (nop, reconstruct_token)
elif args.post_processing == 'tarc-substep2':
    task_processing = tuple([reconstruct_token])
elif args.post_processing == 'tarc-substep3':
    task_processing = tuple([reconstruct_pos])

if len(task_processing) != num_of_tasks:
    raise ValueError(' Expected {} post-processing routines, got {}'.format(num_of_tasks, len(task_processing)))

char_flag = not args.token_sequences
hypotheses = [[] for i in range(num_of_tasks)]
token_dict = {}
ref_totals = [0 for t_idx in range(num_of_tasks)]
hyp_corrects = [0 for t_idx in range(num_of_tasks)]
ref_totals_by_class = [{'arabizi': 0, 'emotag': 0, 'foreign': 0} for t_idx in range(num_of_tasks)]
hyp_corrects_by_class = [{'arabizi': 0, 'emotag': 0, 'foreign': 0} for t_idx in range(num_of_tasks)]
different_lengths = [0 for t_idx in range(num_of_tasks)]
task_wer = [[0.0,0.0,0] for t_idx in range(num_of_tasks)]
for i in range(len(ref_data)):

    ref_outputs = ref_data[i].rstrip(' \r\n').split(sequence_separator)
    hyp_outputs = hyp_data[i].rstrip(' \r\n').split(sequence_separator)

    if len(ref_outputs) != len(hyp_outputs):
        print('   - WARNING, found different number of outputs at {}'.format(i))
        print('   - Reference: {}'.format(ref_data[i]))
        print('   - Hypothesis: {}'.format(hyp_data[i]))

    class_sequence = []
    for t_idx in range(num_of_tasks):
        ref_tokens = task_processing[t_idx]( ref_outputs[t_idx], char_flag )
        if t_idx == 0:
            class_sequence = ref_tokens[:]
        hyp_tokens = task_processing[t_idx]( hyp_outputs[t_idx], char_flag )
        ref_totals[t_idx] += len(ref_tokens)

        if len(ref_tokens) != len(hyp_tokens):
            different_lengths[t_idx] += 1

        for tok_idx in range(min(len(ref_tokens),len(hyp_tokens))):
            ref_totals_by_class[t_idx][class_sequence[tok_idx]] += 1

            if ref_tokens[tok_idx] == hyp_tokens[tok_idx]:
                hyp_corrects[t_idx] += 1
                hyp_corrects_by_class[t_idx][class_sequence[tok_idx]] += 1

        I, D, S, R = compute_wer_from_tensors(ref_tokens, hyp_tokens, token_dict)
        task_wer[t_idx][0] += D+S   # for % token correct
        task_wer[t_idx][1] += I+D+S # for WER
        task_wer[t_idx][2] += R     # Total reference tokens

        if len(hyp_tokens) < len(ref_tokens):
            len_diff = len(ref_tokens) - len(hyp_tokens)
            hyp_tokens = hyp_tokens + [pad_token for i in range(len_diff)]
        elif len(hyp_tokens) > len(ref_tokens): 
            hyp_tokens = hyp_tokens[:len(ref_tokens)]
        hyp_tokens.append( eos_token )
        hypotheses[t_idx].append( hyp_tokens ) 

print(' - compute_mt_accuracy, accuracies computed for {} tasks:'.format(num_of_tasks))
for t_idx in range(num_of_tasks):
    print('   - Accuracy@task {}: {:.2f}%'.format(t_idx+1, float(hyp_corrects[t_idx])*100.0 / float(ref_totals[t_idx])))
    print('     * Accuracy by class:') 
    for k in ref_totals_by_class[t_idx].keys():
        print('       - {:8}: {:.2f}%'.format(k, float(hyp_corrects_by_class[t_idx][k])*100.0 / float(ref_totals_by_class[t_idx][k])))
    print('     * # mismatching hypothesis lengths: {}'.format(different_lengths[t_idx]))
    print('   - % correct @task {}: {:.2f}'.format(t_idx+1, 100.0 - (task_wer[t_idx][0]*100.0/task_wer[t_idx][2])))
    print('   - WER@task {}: {:.2f}%'.format(t_idx+1, task_wer[t_idx][1]*100.0/task_wer[t_idx][2]))
    print('   -----')
    print(' - Saving hypotheses...')
    sys.stdout.flush()
    save_hypotheses( args.hypothesis + '.' + args.save_hypotheses_format + '.level' + str(t_idx), hypotheses[t_idx], args )
    print(' -----')

