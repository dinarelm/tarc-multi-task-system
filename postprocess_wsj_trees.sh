#!/bin/bash

if [[ $# -gt 1 ]]; then
        echo "Usage: $0 <fairseq-generate output file>"; exit;
fi

grep "^S\-" $1 | cut -f2- > $1.input
grep "^T\-" $1 | cut -f2- > $1.ref
grep "^H\-" $1 | cut -f3- > $1.hyp

SCRIPTPATH=/home/getalp/dinarelm/work/tools/fairseq_tools/tarc_multitask_repo/tarc_exps/

python ${SCRIPTPATH}/postprocess_wsj_trees.py --from-seq2seq-format --reverse-input --lexicalized $1.ref $1.hyp
python ${SCRIPTPATH}/flatten_tree.py $1.ref.clean | perl -pe '{s/ +/ /g;}' > tmp
mv tmp $1.ref.clean
python ${SCRIPTPATH}/flatten_tree.py $1.hyp.clean | perl -pe '{s/ +/ /g;}' > tmp
mv tmp $1.hyp.clean

