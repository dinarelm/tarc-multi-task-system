#!/bin/bash

source ${HOME}/work/tools/venv_python3.7.2_torch1.4_decore0/bin/activate
FAIRSEQ_PATH=${HOME}/work/tools/venv_python3.7.2_torch1.4_decore0/bin/
#PYTHONPATH=${HOME}/work/tools/fairseq/

export RNNTAGGERPATH=${HOME}/work/tools/Seq2Biseq_End2EndSLU/

CORPUS='WSJ-2tasks-toks-and-chars-lexGAFLtrees'
DATA_FORMAT='parallel'
#DATA_PATH=${HOME}/work/data/Arabish/UltimiDati/Tokenization-POSTagging/Step6-Ex/MADAR-complete_TArC-Block1-6-Train.simple-ex.clean.tab.npos
#DATA_PATH=${HOME}/work/data/Arabish/UltimiDati/Tokenization-POSTagging/AutoIncrementData_from-Ar/MADAR.complete-with-arabish+TArCblock1-6.tab.Ar-Cl-Tk-NPos-Pos
DATA_PATH=${HOME}/work/data/WSJ/span-parser/data/2-tasks/wsj
#DATA_PATH=${HOME}/work/data/tiger-2cols/tiger_release_aug07.clean.txt.multi-task
#DATA_PATH=${HOME}/work/data/tiger-2cols/tiger2_release_aug07.clean.txt.tab.morpho
SERIALIZED_DATA=./system_inputs/${CORPUS}.multi-task.data.${DATA_FORMAT}

LR=0.0005	# Best with LSTM: 0.0005
LR_SHRINK=0.96	# Best with LSTM: 0.96
DROPOUT=0.5	# Best with LSTM: 0.4
CLIP_NORM=5.0
WDECAY=0.0001
BATCH=10		# Best with LSTM: 5
MAX_EPOCHS=120
encoder_embed=$((256))
decoder_embed=${encoder_embed}
encoder_size=${encoder_embed}
decoder_size=${encoder_embed}

ENC_LAYERS=3
ATT_HEADS=2
ENC_FFN_DIM=$((2*$encoder_size))
DEC_LAYERS=3

MODEL_TYPE='lstm' # lstm or transformer
SUBTASK='wsj'
GRN_OPTIONS='--char-sequences --token-sequences'	# --token-sequences or --char-sequences, or both
CRITERION='tarc_multitask_loss'

ANNEAL=1
WU_EPOCHS=0
UPDATES_PER_EPOCH=1222 # MADAR+Tarc with batch size 5
WU_UPDATES=$((${WU_EPOCHS}*${UPDATES_PER_EPOCH}))
WARMUP_OPTION="" #"--warmup-updates ${WU_UPDATES}"	# Best with LSTM: no warmup
DIM_OPTIONS="--encoder-embed-dim ${encoder_embed} --encoder-hidden-dim ${encoder_size} --decoder-embed-dim ${decoder_embed} --decoder-hidden-dim ${decoder_size} --decoder-out-embed-dim ${decoder_embed}"
REGULARIZATION_OPTIONS="--clip-norm ${CLIP_NORM} --weight-decay ${WDECAY}"
if [[ ${MODEL_TYPE} == "transformer" ]]; then
	DIM_OPTIONS="--encoder-embed-dim ${encoder_size} --decoder-embed-dim ${decoder_size}"
	REGULARIZATION_OPTIONS=""
	WARMUP_OPTION="--warmup-updates ${WU_UPDATES}"
fi

SAVE_PATH_PREF=${CORPUS}-${DATA_FORMAT}_BATCH${BATCH}_${ENC_LAYERS}xH${encoder_embed}-FFNN${ENC_FFN_DIM}-${ATT_HEADS}Heads_LR${LR}_Shr${LR_SHRINK}_Dec${WDECAY}_Drop${DROPOUT}_${MODEL_TYPE}_NormBef_WU${WU_EPOCHS}Epo_XAttnCat_FullDrop_ToksAndCharsInput_lexGAFLtrees_FID_TEST/
#CUDA_VISIBLE_DEVICES=1
PYTHONPATH=${HOME}/work/tools/fairseq/ ${FAIRSEQ_PATH}/fairseq-train ${DATA_PATH} --reverse-input --force-input-decoding --tree-format GAFL \
	--task tarc_multitask --arch tarc_multitask_arch --sub-task ${SUBTASK} --model-type ${MODEL_TYPE} --criterion ${CRITERION} --num-workers=0 --distributed-world-size 1 ${GRN_OPTIONS} \
	--pad-reference --save-dir ${SAVE_PATH_PREF} --serialized-data ${SERIALIZED_DATA} --data-format ${DATA_FORMAT} --patience 20 --no-epoch-checkpoints \
	--encoder-normalize-before --encoder-layers ${ENC_LAYERS} --encoder-attention-heads ${ATT_HEADS} --encoder-ffn-embed-dim ${ENC_FFN_DIM} \
	--share-all-embeddings --activation-dropout ${DROPOUT} --attention-dropout ${DROPOUT} \
	--decoder-normalize-before --decoder-layers ${DEC_LAYERS} ${DIM_OPTIONS} \
	--dropout ${DROPOUT} --decoder-attention-heads ${ATT_HEADS} \
	--optimizer adam --lr ${LR} --force-anneal ${ANNEAL} --lr-shrink ${LR_SHRINK} ${WARMUP_OPTION} ${REGULARIZATION_OPTIONS} \
	--max-sentences ${BATCH} --max-epoch ${MAX_EPOCHS} --keep-best-checkpoints 5

