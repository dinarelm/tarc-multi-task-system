
import os
import sys

f = open(sys.argv[1], encoding='utf-8')
lines = f.readlines()
lines = [l.rstrip() for l in lines]
f.close()

tree = ''
for l in lines: 
    if len(l) == 0:
        print(tree)
        tree = ''
    else:
        tree = tree + l

