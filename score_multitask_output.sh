#!/bin/bash

if [[ $# -gt 2 ]]; then
	echo "Usage: $0 <fairseq-generate output file> [post-processing mode]"; exit;
fi

POST_PROCESSING='none'
if [[ $# -eq 2 ]]; then
	POST_PROCESSING=$2
fi

grep "^T\-" $1 | cut -f2- > $1.ref
grep "^H\-" $1 | cut -f3- > $1.hyp

SCRIPT_PATH=${HOME}/work/tools/fairseq_tools/tarc_multitask_repo/tarc-multi-task-system

# NOTE: add --token-sequences to options to score token-level sequences, or character-level sequences without reconstructing tokens
OPTIONS="--post-processing ${POST_PROCESSING}"
python ${SCRIPT_PATH}/compute_mt_accuracy.py ${OPTIONS} $1.ref $1.hyp

