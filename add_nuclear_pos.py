
import os
import sys

sys.path.append( '/home/getalp/dinarelm/work/tools/fairseq/fairseq/' )

import torch
import argparse
from tarc_utils import *
from typing import List

parser = argparse.ArgumentParser(description='Add a nuclear (i.e. basic) POS tag to the data in the specified position')
parser.add_argument('data', help='Input file in tabular format')
parser.add_argument('--input-idx', type=int, default=-1, help='The column index for reading the POS tag')
parser.add_argument('--output-idx', type=int, default=-1, help='The column index where to add the nuclear POS tag')
args = parser.parse_args()

def extract_nuclear_pos(POS):

    npos = POS
    if ('IV' in POS or 'CV' in POS or 'PV' in POS or 'VERB' in POS) and not 'NOUN' in POS:
        npos = 'VV'
    elif ('IV' in POS or 'CV' in POS or 'PV' in POS or 'VERB' in POS) and 'NOUN' in POS:
        npos = 'VN'
    elif 'NOUN' in POS:
        npos = 'NOUN'
    elif 'PRON' in POS:
        npos = 'PRON'
    elif 'ADJ' in POS:
        npos = 'ADJ'
    elif 'ADV' in POS:
        npos = 'ADV'
    elif 'PART' in POS:
        npos = 'PART'
    elif 'CONJ' in POS:
        npos = 'CONJ'

    return npos

def main(args):

    with open(args.data, 'rb') as f:
        lines = [l.decode('utf-8') for l in f.readlines()]
        f.close()

    data = []
    for line in lines:
        tokens = line.rstrip(' \r\n').split('\t')

        if len(tokens) > 1:
            pos = tokens[args.input_idx]
            n_pos = extract_nuclear_pos(pos)
            tokens = tokens[:args.output_idx] + [n_pos] + tokens[args.output_idx:]
            data.append(tokens)
        else:
            data.append([])

    f = open(args.data + '.npos', 'w', encoding='utf-8')
    for d in data:
        if len(d) > 0:
            f.write('\t'.join(d) + "\n")
        else:
            f.write("\n")
    f.close()

if __name__ == '__main__':
    main(args)

