#!/bin/bash

source ${HOME}/work/tools/venv_python3.7.2_torch1.4_decore0/bin/activate
FAIRSEQ_PATH=${HOME}/work/tools/venv_python3.7.2_torch1.4_decore0/bin/
#PYTHONPATH=${HOME}/work/tools/fairseq/

export RNNTAGGERPATH=${HOME}/work/tools/Seq2Biseq_End2EndSLU/

if [[ $# -ne 2 ]]; then
	echo "Usage: $0 <model file> <data file>"; exit
fi

# NOTE: you can set this to anything since data are provided as serialized in $2. This is needed just because it is a mandatory argument for fairseq-generate
DATA_PATH=${HOME}/work/tools/fairseq_tools/tarc_multitask_repo/tarc-multi-task-system/data/tiger/parallel_format/tiger_release_aug07.clean.txt.multi-task

SERIALIZED_DATA=$2
CHECKPOINT=$1

SUBTASK='tarc-lemma'
GRN_OPTIONS="--char-sequences"
GENERATE_OPTIONS="--beam 10 --iter-decode-max-iter 10 --max-len-a 1.5 --max-len-b 25 --prefix-size 0"

# NOTE: for the moment only --max-sentences 1 is supported. Multi-tasking creates constraints in the way of managing tensors at testing time that I have not gone through yet.
# --max-sentences 1 --keep-data-order # For annotating data
CHECKPOINT_DIR=`dirname ${CHECKPOINT}`
#CUDA_LAUNCH_BLOCKING=1
CUDA_VISIBLE_DEVICES=1 PYTHONPATH=${HOME}/work/tools/fairseq/ ${FAIRSEQ_PATH}/fairseq-generate ${DATA_PATH} --max-sentences 20 \
	--path ${CHECKPOINT} --serialized-data ${SERIALIZED_DATA} ${GENERATE_OPTIONS} --num-workers=0 \
	--task tarc_multitask --sub-task ${SUBTASK} ${GRN_OPTIONS} \
	--gen-subset valid --results-path ${CHECKPOINT_DIR} \

exit
CUDA_VISIBLE_DEVICES=1 PYTHONPATH=${HOME}/work/tools/fairseq/ ${FAIRSEQ_PATH}/fairseq-generate ${DATA_PATH} --max-sentences 1 --keep-data-order \
        --path ${CHECKPOINT} --serialized-data ${SERIALIZED_DATA} ${GENERATE_OPTIONS} --num-workers=0 \
        --task tarc_multitask --sub-task ${SUBTASK} ${GRN_OPTIONS} \
        --gen-subset test --results-path ${CHECKPOINT_DIR} \
