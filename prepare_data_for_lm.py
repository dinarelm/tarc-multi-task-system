
import os
import sys
import argparse
import random

from fairseq.tarc_utils import *

sequence_separator = '__SEP__'

def read_tabular_data(filename):

    f = open(filename, encoding='utf-8')
    lines = f.readlines()
    f.close()
    data = [l.strip() for l in lines]

    ncolumns = -1
    sequences = []
    cur_sequences = []
    for line in data:
        tokens = line.rstrip(' \r\n').split('\t')

        if len(tokens) > 1:
            if ncolumns == -1:
                ncolumns = len(tokens)
            elif ncolumns != len(tokens):
                raise ValueError('Found different number of columns at different lines ({} vs. {})'.format(ncolumns, len(tokens)))
            if len(cur_sequences) == 0:
                cur_sequences = [[] for i in range(len(tokens))]
            for i in range(len(tokens)):
                cur_sequences[i].append(tokens[i])
        else:
            sequences.append( cur_sequences )
            cur_sequences = []

    print(' * Read {} sequences with {} columns'.format(len(sequences), ncolumns))
    sys.stdout.flush()

    return sequences

def read_data(args, filename):

    if args.data_format == 'text':
        f = open(filename, encoding='utf-8')
        lines = f.readlines()
        f.close()
        tmp = [l.strip().split() for l in lines]
        data = [[s] for s in tmp]
    elif args.data_format == 'tabular':
        data = read_tabular_data(filename)
    else:
        raise NotImplementedError

    if args.items == 'token':
        return data
    elif args.items == 'char':
        raise NotImplementedError
    else:
        raise NotImplementedError

def mask_tokens(args, sequence):

    masking_partition = [0.8, 0.9, 1.0]

    if args.char_level_lm:
        lengths = [len(t) for t in sequence]
        seps = []
        for i, t in enumerate(sequence):
            if t == sequence_separator:
                seps.append(i)
        seps_range = []
        start = 0
        for i, l in enumerate(lengths):
            if i in seps:
                seps_range = seps_range + list(range(start, start+l))
            start += l
        tokens = list(''.join(sequence))
        mask = '¤'  # NOTE: hopefully this does not occur in the data...


        '''print(' * Sequence: {}'.format(sequence))
        print(' * lengths: {}'.format(lengths))
        print(' * seqs: {}'.format(seps))
        print(' * seps_range: {} (-> {})'.format(seps_range, ''.join([tokens[i] for i in seps_range])))
        sys.stdout.flush()'''
    else:
        tokens = sequence
        mask = '[MASK]' 


    orig_tokens = tokens[:]
    idx = list(range(len(tokens))) 
    random.shuffle(idx)
    mask_idx = idx[:int(len(tokens) * args.masking_ratio)]
    for i in mask_idx:
        if i not in seps_range:
            luck = random.random()
            if luck <= masking_partition[0]:
                #print('[DEBUG] masking {} -> {}...'.format(tokens[i], mask))
                #sys.stdout.flush()

                tokens[i] = mask

            elif luck > masking_partition[0] and luck <= masking_partition[1]:
                tokens[i] = tokens[i]

                #print('[DEBUG] nop @ {}!'.format(tokens[i]))
                #sys.stdout.flush()
            else:
                rand_i = random.randint(0, len(tokens)-1) 

                #print('[DEBUG] random replacement {} -> {}...'.format(tokens[i], tokens[rand_i]))
                #sys.stdout.flush()

                tokens[i] = orig_tokens[rand_i]

    if args.char_level_lm:
        res = []
        prev = 0
        for l in lengths:
            res.append(''.join(tokens[prev:prev+l]))
            prev += l
        return res
    else:
        return tokens

def process_data(args, data):

    output_data = []
    for si in range(args.nsentences-1, len(data)):
        cur_sequences = []
        in_seq = []

        #print('* Processing: {}'.format(data[si][0]))

        for sii in range(args.nsentences-1,-1,-1):
            #print(' * Adding: {}'.format(data[si-sii][0]))

            in_seq = in_seq + data[si-sii][0] + [sequence_separator]
        in_seq = in_seq[:-1]
        if len(in_seq) < args.shift:
            in_seq = in_seq + [eos_token] * (args.shift - len(in_seq))
        
        #print('* Final in_seq: {}'.format(in_seq))
        #sys.stdout.flush()

        if args.masked_lm:
            out_seq = in_seq[:]
            in_seq = mask_tokens(args, in_seq)
            if args.mask_unchanged_tokens:
                for i in range(len(in_seq)):
                    if in_seq[i] == out_seq[i]:
                        out_seq[i] = 'O'
        else:
            #cur_sequences.append(in_seq)
            out_seq = in_seq[args.shift:] + [eos_token] * args.shift
            assert len(in_seq) == len(out_seq), 'Input and output sequences must have the same length, got ({} vs. {}): {} VS. {}'.format(len(in_seq), len(out_seq), in_seq, out_seq)

        #print('* Final out_seq: {}'.format(out_seq))
        #sys.stdout.flush()

        #cur_sequences.append(out_seq)

        output_data.append( [in_seq, out_seq] )
    return output_data

def save_output_data(data, filename):

    '''print('1. {}'.format(data[0]))
    print(' ---')
    print('2. {}'.format(data[0][0]))
    print(' ---')
    sys.exit(0)'''

    f = open(filename, 'w', encoding='utf-8')
    for ss in data:

        '''print('Printing ss: {}'.format(ss))
        print(' ---')
        print(' Len of ss[0]: {}'.format(len(ss[0])))
        print(' ---')'''

        for idx in range(len(ss[0])):
            for sidx in range(len(ss)):
                #print('Printing s[]: {}')

                if sidx < len(ss)-1:
                    f.write(ss[sidx][idx] + '\t')
                else:
                    f.write(ss[sidx][idx] + '\n')
        f.write('\n')
    f.close()

def main():

    parser = argparse.ArgumentParser(description='Prepare data for training language models with the multi-task system')
    parser.add_argument('data', help='Input data')
    parser.add_argument('--data-format', type=str, default='text', help='Expected data format: 1) text (1 sentence per line); 2) tabular (1 token per line, possibly on multiple columns, sequences separated by 1 empty line, columns separated by tabs)')
    parser.add_argument('--items', type=str, default='token', help='Items to be used in the LM: 1) token (single tokens); 2) char (single characters)')
    parser.add_argument('--masked-lm', action='store_true', default=False, help='Perform masking of input tokens for masked lm training')
    parser.add_argument('--masking-ratio', type=float, default=0.15, help='Ratio of input tokens to mask')
    parser.add_argument('--mask-unchanged-tokens', action='store_true', default=False, help='Replace unchanged output tokens with a conventional symbol so that the model only learns to recostruct masked tokens')
    parser.add_argument('--char-level-lm', action='store_true', default=False, help='Perform masking at character level')
    parser.add_argument('--shift', type=int, default=1, help='Shift on predicted items')
    parser.add_argument('--nsentences', type=int, default=2, help='Number of concatenated sentences')
    parser.add_argument('--use-features', action='store_true', default=False, help='Use also other information when given in a tabular input file')
    parser.add_argument('--output-format', type=str, default='tabular', help='Format of the output file to be used for training/dev')
    parser.add_argument('--output-file', type=str, help='Name of the output file (default: <input file>.lm')
    args = parser.parse_args()

    data = read_data(args, args.data)
    output_data = process_data(args, data)

    output_file = args.output_file if args.output_file else args.data + '.lm'
    save_output_data(output_data, output_file)

if __name__ == '__main__':
    main()

