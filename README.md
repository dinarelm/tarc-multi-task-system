<div align="center">
<p>
  <img src="docs/fairseq_logo.png" width="150">
  <br />
  <br />
  <a href="https://creativecommons.org/licenses" /><img src="docs/cc_attribution_0.png" width="75"></a>
</p>
</div>

--------------------------------------------------------------------------------

# Tarc Multi-Task System (TarcMTS)

TArC Multi-Task System is a [Fairseq](https://github.com/pytorch/fairseq) system designed originally for the [TArC multi-level annotated corpus](https://github.com/eligugliotta/tarc).
The system however is generic so it can be used for any sequence-to-sequence problem, like e.g. machine translation, sequence tagging, etc.

The intended way of using TarcMTS is for cascaded multi-tasking.
That is given one input $`I`$ and one or more outputs $`O_1 ... O_N`$, the system trains to predict $`O_i`$ given $`I`$, $`O_1 ... O_{i-1}`$.
If only one output is given, the system reduces to a normal sequence-to-sequence framework.

In the case of the [TArC corpus](https://github.com/eligugliotta/tarc), several level of annotations are available (they are actually being annotated at this moment...), like tokenization level, POS tags, transcription into Tunisian dialect, and eventually lemmas.
The system thus can be trained to predict Tunisian transcriptions given the input (Arabish text), and all the other annotation levels, which makes transcription quality much better.

In the case of machine translation, the system can train from one source language and several target languages, creating possibly good multi-language embeddings in the encoder, and producing more accurate translations for each target language. Machine translation tests have not been performed yet however.

### Features

- TarcMTS trains [LSTM](https://colah.github.io/posts/2015-08-Understanding-LSTMs/) and [Transformer](http://jalammar.github.io/illustrated-transformer/) models.
- Both parallel and tabular data format are supported
- Training with token-level information, character-level information, or both, on input and output side
- The system takes text input and generates dictionaries on its own. Data and dictionaries are saved in separate files with **torch.save** so that to be *ready-to-use* for the next run. **--serialized-data** option allows to specify the common prefix for all files. Five different files will be generated: .train, .dev, .test, .dict and .token2components. The first four are self-explenatory, the last contains information for generating characters or components for whole tokens and viceversa.

### Installation

The system was developped under ***python 3.7, pytorch 1.9.0 (previously 1.4.0) and Fairseq 0.9***, it can probably work with other versions of pytorch, but it has never been tested for it. For sure the system will not work with more recent versions of Fairseq.

The fairseq installation used for the system has been added into the repository in mai 2022, the code should be in the exact same version as used for our new paper at LREC 2022 (coming soon).
For installation you should now just type:

```code
cd fairseq/
pip install -e .
```

The other scripts are used for training, generation, and evaluation (see below).
Some scripts have been added also for performing constituency syntactic parsing on the PTB corpus (more details coming soon...).

### Usage

#### Training

In order to train models, you can use the **run_tarc_multitask_train.sh** script.
Take a look to environment and other variables and modify them according to your settings and needs.
This script will run ***fairseq-train***, the model will be saved in a directory specified as a variable in the script.

Some data are provided in the **data folder** of this repository in order to test the system.
Such data and the code have been slightly updated (TArC data in particular have been anonimized) with respect to what was used for our [WANLP 2020 paper](https://arxiv.org/abs/2011.05152), thus results may be slightly different. The overall message in the paper does not change however.

Attention should be payed to the following options:

- **--sub-task**: specifies how input and outputs are processed by the system.
- - **base** (or **none**) can be used with any data, the system will treat all input and output sequences at token or character level depending on the ***--token-sequences*** and ***--character-sequences*** options.
- - **tiger-mt** can be used with the multi-task setting for the TIGER corpus as explained in our [WANLP 2020 paper](https://arxiv.org/abs/2011.05152)
- - **tiger-mt-ext** is like **tiger-mt**, except that morpho-syntactic labels in TIGER will be split in to components
- - **tarc-full** can be used with the multi-task setting for the [TArC corpus](https://github.com/eligugliotta/tarc) as explained in our [WANLP 2020 paper](https://arxiv.org/abs/2011.05152)
- **--token-sequences**: specifies to use token-level sequences
- **--char-sequences**: specifies to use character-level sequences

If both **--token-sequences** and **--char-sequences** are used, the system will use both token and character level information for input and output sequences.

**PLEASE NOTE** that **--sub-task**, and the usage of **--token-sequences** and/or **--char-sequences** will affect possibly how data will be saved in the files which prefix is specified with **--serialized-data**.
These options should thus be coherent between training and generation phase. Also at evaluation phase the post-processing mode should be set according to the **--sub-task** option (see below in the Evaluation section).

#### Generation

In order to run generation to produce automatic outputs, you can use the **run_tarc_multitask_test.sh** script.
Again, environment and other variables must be modified.
This script will run ***fairseq-generate*** to produce outputs.
Outputs corresponding to different level of annotation or languages, will be separated by a special token (***\_SEQ_SEP\_*** by default).
```
run_tarc_multitask_test.sh <checkpoint file> <data file>
```

The **\<data file\>** argument is the same prefix as used for training (if you ran training first, see above). If the files expected with the specified prefix do not exist, the system will read data and save the files before performing the generation.
Please note that in order to be coherent with the training phase, for the generation phase options **--sub-task**, **--token-sequences** and **--char-sequences** should be the same as for the training phase.

#### Evaluation

The script **score_multitask_output.sh** can be used to evaluate the predicted outputs against gold references 
in case all outputs predicted by the model can be scored with accuracy or Word Error Rate (WER). Also % of correct tokens is given, as well as the number of predicted sequences which length doesn't match the reference length.
If you need another evaluation metric, you need to write your own evaluation script.
This script is basically just a wrapping for **compute_mt_accuracy.py**.
```
score_tarc_multitask_model.sh <generated-outputs> <post-processing mode>
```

**\<post-processing mode\>** specifies how to post-process outputs to reconstruct whole tokens from characters/components:
- **none**: no post-processing will be performed
- **tiger**: post-processing for the TIGER corpus as explained in our [WANLP 2020 paper](https://arxiv.org/abs/2011.05152)
- **tiger-ext**: post-processing for the TIGER corpus where morpho-syntactic labels will be reconstructed from components
- **tarc-full**: post-processing for the [TArC corpus](https://github.com/eligugliotta/tarc) as explained in our [WANLP 2020 paper](https://arxiv.org/abs/2011.05152)

# License

TarcMTS is CC-BY licensed.
The license applies to all files provided in this repository as well.

# Citation

Please cite as (this will be updated as soon as the proceedings will be available):

```bibtex
@inproceedings{gugliotta-etal-wanlp2020,
  title={Multi-Task Sequence Prediction For Tunisian Arabizi Multi-Level Annotation},
  author={Gugliotta, Elisa and Dinarelli, Marco and Kraif, Olivier},
  booktitle={The Fifth Arabic Natural Language Processing Workshop (WANLP)},
  year={2020},
}
```
