#!/bin/bash

if [[ $# -ne 3 ]]; then
	echo "Usage: $0 <bad hyp. trees> <bad hyp. words> <bad ref. trees>"; exit
fi

python postprocess_wsj_badtrees.py $1 $2

cat $1.lex | perl -pe '{s/\( /\(/g; s/ \)/\)/g;}' > tmp
mv tmp $1.lex
cat $3 | perl -pe '{s/\( /\(/g; s/ \)/\)/g;}' > tmp
mv tmp $3

