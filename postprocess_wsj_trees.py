
import os
import sys
import argparse
from nltk.tree import Tree

SEQ_SEP='_SEQ_SEP_'

def read_sequences(filename):

    f = open(filename, encoding='utf-8')
    lines = f.readlines()
    f.close()

    return [l.strip() for l in lines]

def save_sequences(filename, sequences, pad_flag=False):

    f = open(filename, 'w', encoding='utf-8')
    for s in sequences:
        print(s, file=f)
        if pad_flag:
            print('', file=f)
    f.close()

def rebalance_tree(tree):
    
    stack = []
    for c in tree.split():
        if c != ')':
            stack.append(c)
        elif c == ')':
            idx = len(stack)-1
            if len(stack) <= 0:
                print(' *** Unbalanced tree: {}'.format(tree))
                sys.stdout.flush()
                return
            while idx >= 0 and stack[idx] != '(':
                idx -= 1 
            if idx < 0:
                print(' *** Unbalanced tree 1): {}'.format(' '.join(stack)))
                sys.stdout.flush()
                return
            else:
                #print(' * Matched: {}'.format(' '.join(stack[idx:])))
                stack = stack [:idx]
                #print(' * Stack: {}'.format(stack))
    if len(stack) > 0:
        print(' *** Unbalanced tree 2): {}'.format(tree))
        sys.stdout.flush()
    #sys.exit(0)

def replace_leaves(tree, new_leaves):

    for i in range(len(tree)):
        if isinstance(tree[i], Tree):
            new_leaves = replace_leaves(tree[i], new_leaves)
        else: 
            assert isinstance(tree[i], str) 
            #assert len(new_leaves) > 0
            tree[i] = new_leaves[0]
            new_leaves = new_leaves[1:]
     
    return new_leaves

def remove_leaf(tree, leaf):

    path = None
    if isinstance(tree, Tree) and len(tree) == 1 and tree[0] == leaf:
        #path.append(tree)
        print(' * Found leaf >{}<: {}'.format(leaf, tree))
        return tree
    elif isinstance(tree, Tree) and len(tree) == 1:
        subpath = remove_leaf(tree[0], leaf)
        if subpath is not None:
            print(' ** Found path to leaf >{}<: {}'.format(leaf, tree))
            return tree
        return None
    elif isinstance(tree, Tree):
        for i in range(len(tree)):
            subpath = remove_leaf(tree[i], leaf)
            if subpath is not None:
                return subpath
    else:
        assert isinstance(tree, str) and tree != leaf
    return path

def remove_leaves(tree, leaves):

    for lf in leaves:
        path = remove_leaf(tree, lf)
        if len(path) > 0:
            print(' * remove_leaves, got path to leaf {}:'.format(lf))
            print(path)
            print(' -----')
        else:
            print(' * WARNING: leaf {} not found in tree {}'.format(lf, tree))
        sys.exit(0)

def add_subtree(tree, leaves):

    children = []
    for lf in leaves:
        children.append( Tree('NN', [lf]) )
    add_tree = Tree('NP', children)
    return Tree(tree.label(), tree[:] + [add_tree])

def convert_from_seq2seq_format( tree_str, POS_dict=None ):

    NT_stack = []
    new_toks = []
    pos_flag = False
    toks = tree_str.strip().split()
    for i in range(len(toks)):
        if '(' in toks[i]:
            curr_NT = toks[i][1:]
            new_toks.append( toks[i][0] + ' ' + toks[i][1:] )
            NT_stack.append( curr_NT )

            if i > 0 and len(NT_stack) == 0:
                print(' WARNING Processing tree: {}'.format(tree_str))
                print('   * Detected left out components: {}'.format(toks[i:]))
                sys.stdout.flush()

            #if i < len(toks)-1 and not ('(' in toks[i+1] or ')' in toks[i+1]) and toks[i+1] in POS_dict:
            #    new_toks.append('(')
            #    pos_flag = True
        elif ')' in toks[i]:
            curr_NT = toks[i][1:]
            #new_toks.append( toks[i][0] )
            if len(NT_stack) > 0 and curr_NT != NT_stack[-1]:
                print(' WARNING Processing tree: {}'.format(tree_str))
                print('   * Expected non-terminal {}, got {}'.format(NT_stack[-1], curr_NT))
                print(' ----------')
                sys.stdout.flush()
  
                NT_stack.pop()
                new_toks.append( toks[i][0] )

            elif len(NT_stack) == 0:
                print(' WARNING Processing tree: {}'.format(tree_str))
                print('   * Early closed non-terminal {}'.format(curr_NT))
                print(' ----------')
                sys.stdout.flush() 
            else:
                assert curr_NT == NT_stack[-1]
                NT_stack.pop()
                new_toks.append( toks[i][0] )

            #if i < len(toks)-1 and not ('(' in toks[i+1] or ')' in toks[i+1]) and toks[i+1] in POS_dict:
            #    new_toks.append('(')
            #    pos_flag = True
        else:
            if POS_dict is not None:
                new_toks.append('(')
            
            new_toks.append( toks[i] )
            
            if POS_dict is not None:
                new_toks.append(')') 
                assert toks[i] in POS_dict, ' {} not defined in the POS dictionary'.format(toks[i])

            #if pos_flag and i < len(toks)-1 and ('(' in toks[i+1] or ')' in toks[i+1]):
            #    new_toks.append(')')
            #    pos_flag = False

    if len(NT_stack) > 0:
        print(' WARNING Processing tree: {}'.format(tree_str))
        print('   * Unmatched non-terminal(s): {}'.format(NT_stack))
        print(' ===============')
        sys.stdout.flush()
        for nt in NT_stack:
            new_toks.append(')')
        #raise ValueError( ' Unmatched non-terminal(s): {}'.format(NT_stack) )

    tree = ' '.join(new_toks)

    return tree

def add_lexical_level(tree, words, debug_flag=False):

    if isinstance(tree, Tree) and len(tree) == 0:
        #tree.children.append(words[0])
        new_tree = Tree(tree.label(), [words[0]])

        if debug_flag:
            print(' #DEBUG, POS and lexical level: ({} {}) ( ### {} ### )'.format(tree.label(), words[0], new_tree))
            sys.stdout.flush()

        return new_tree, words[1:]
    elif isinstance(tree, Tree): 
        for c_idx in range(len(tree)):
            child = tree[c_idx]
            #print('child type: {}; words type: {}'.format(type(child), type(words)))
            #sys.stdout.flush()

            new_child, words = add_lexical_level(child, words, debug_flag)
            tree[c_idx] = new_child
            if len(words) == 0 and c_idx < len(tree)-1:
                print(' * add_lexical_level WARNING: forcing return before finishing tree span')
                return tree, []
        return tree, words

def tree_to_string(tree):

    tree_str = ''
    if isinstance(tree, Tree):
        tree_str = tree_str + '( ' + tree.label() + ' '
        for child in tree:
            tree_str = tree_str + tree_to_string( child )
        tree_str = tree_str + ' )'
    elif isinstance(tree, str):
        tree_str = tree_str + tree

    return tree_str

def main():

    parser = argparse.ArgumentParser(
        description='Post-process trees of WSJ format for performing constituent syntactic parsing evaluation'
    )
    parser.add_argument('ref', help='File containing the reference trees')
    parser.add_argument('hyp', help='File containing the hypothesis trees (the system output)')
    parser.add_argument('--from-seq2seq-format', action='store_true', default=False,
                        help='Convert the trees from the linear format for sequence-to-sequence parsing')
    parser.add_argument('--pos-dict', type=str, help='Load the POS dictionary from the specfied file')
    parser.add_argument('--source', type=str, help='Input sequences for reconstructing trees with lexical level from the seq2seq format')
    parser.add_argument('--lexicalized', action='store_true', default=False, help='Indicates that trees are already lexicalized.')
    parser.add_argument('--reverse-input', action='store_true', default=False, help='Reverse input sequence order (as used for seq2seq syntactic parsing)')
    args = parser.parse_args()

    f = open(args.ref, encoding='utf-8')
    lines = f.readlines()
    f.close()

    pos_dict = None
    if not args.lexicalized:
        pos_dict = {}
        pos_dict['UH'] = 1
        pos_dict['FW'] = 1
        pos_seqs = [l.split(SEQ_SEP)[0] for l in lines]
        for ps in pos_seqs:
            for pos in ps.strip().split():
                if pos not in pos_dict:
                    pos_dict[pos] = 1

    if args.source:
        f = open(args.source, encoding='utf-8')
        src_lines = f.readlines()
        f.close()
        sources = [l.strip().split() for l in src_lines]
        if args.reverse_input:
            for idx, s in enumerate(sources):
                sources[idx].reverse()

        print(' * Read {} source sequences'.format(len(sources)))
        sys.stdout.flush()

    lines = [l.split(SEQ_SEP)[-1] for l in lines]
    if args.from_seq2seq_format:
        assert (args.source and not args.lexicalized) or (args.lexicalized and not args.source)

        for l_idx in range(len(lines)):
            #print(' Read tree: {}'.format(lines[l_idx].strip()))
            #sys.stdout.flush()

            lines[l_idx] = convert_from_seq2seq_format( lines[l_idx].strip(), pos_dict )

            #print(' Converted tree: {}'.format(lines[l_idx]))
            #sys.stdout.flush()

    #debug_idx = 0
    reftrees = [Tree.fromstring(l.rstrip()) for l in lines]
    if args.from_seq2seq_format and args.source:
        assert len(reftrees) == len(sources)
        for idx, t in enumerate(reftrees):
            #print(' * Processing tree: {}'.format(t))
            #sys.stdout.flush()

            tree, rem = add_lexical_level(t, sources[idx])
            if len(rem) > 0:
                ValueError(' Malformed reference tree {}, no way to add the following words: {}'.format(reftrees[idx], rem))
            reftrees[idx] = tree

            #print(' * Added leaves: {}'.format(tree))
            #print(' * Remaining leaves: {}'.format(rem))
            #debug_idx += 1
            #if debug_idx >= 10:
            #    sys.exit(0) 

    # Save reconstruced reference trees in order to compare them with the original reference trees
    f = open(args.ref + '.trees.reconstructed', 'w', encoding='utf-8')
    for t in reftrees:
        print(t, file=f)
        print('', file=f)
    f.close()

    #print(' SO FAR SO GOOD!')
    #sys.exit(0)

    debug_idx = []
    debug_tokens = [] #'Health and Human Services Secretary Louis Sullivan'.split()

    f = open(args.hyp, encoding='utf-8')
    lines = f.readlines()
    f.close()
    bad_trees_idx = []
    bad_trees = []
    bad_oldtrees = []
    bad_ref = []
    bad_sources = []
    hyptrees = []
    hypsources = []
    lines = [l.split(SEQ_SEP)[-1] for l in lines]
    hyp_idx = 0
    for idx in range(len(lines)):
        line = lines[idx].strip()

        if args.from_seq2seq_format:

            old_line = line
            line = convert_from_seq2seq_format( line, pos_dict )

        if line[-1] != ')':
            print(' Malformed hypothesis tree: {}'.format(line))
            sys.stdout.flush()
            bad_trees_idx.append(idx)
            bad_trees.append( line )
            bad_oldtrees.append( old_line )
            bad_ref.append( tree_to_string( reftrees[idx] ) )
            if not args.lexicalized:
                bad_sources.append( ' '.join( sources[idx] ) )
        else:
            try:

                htree = Tree.fromstring( line )
                hyptrees.append( htree )
                if not args.lexicalized:
                    hypsources.append( sources[idx] )

                if hyp_idx in debug_idx: 
                    print(' #DEBUG{}, converting tree: {}'.format(hyp_idx+1, old_line))
                    print(' #DEBUG{}, converted tree: {}'.format(hyp_idx+1, line)) 
                    sys.stdout.flush()

                hyp_idx += 1

            except BaseException as err:
                print(' Parsing ERROR at {}'.format(line) )
                bad_trees_idx.append( idx )
                bad_trees.append( line )
                bad_oldtrees.append( old_line )
                bad_ref.append( tree_to_string( reftrees[idx] ) )
                if not args.lexicalized:
                    bad_sources.append( ' '.join( sources[idx] ) )

    if args.from_seq2seq_format and args.source:
        for idx, t in enumerate(hyptrees):

            # BEGIN DEBUG STUFF
            found = True
            if len(debug_tokens) == 0:
                found = False
            for tok in debug_tokens:
                if tok not in hypsources[idx]:
                    found = False
                    break

            if found:
                print(' #DEBUG{}: found corresponding index to {}'.format(idx, debug_idx))
                sys.stdout.flush()
            # END DEBUG STUFF

            dflag = idx in debug_idx
            if dflag:
                print(' #DEBUG{}, adding words: {}'.format(idx+1, hypsources[idx]))
                sys.stdout.flush()
 
            tree, rem = add_lexical_level(t, hypsources[idx], debug_flag=dflag)
            if len(rem) > 0:
                sys.stderr.write(' Malformed hypothesis tree {}, no way to add the following words: {}\n'.format(hyptrees[idx], rem))
                sys.stdout.flush()
            hyptrees[idx] = tree

            if dflag:
                print(' #DEBUG{}, lexicalized tree: {}'.format(idx+1, tree_to_string(tree)))
                sys.stdout.flush()

    if len(bad_trees_idx) > 0:
        print(' - WARNING: detected {} malformed trees in hypotheses, removing corresponding reference trees'.format(len(bad_trees_idx)))
        sys.stdout.flush()

        newlist = []
        for idx in range(len(reftrees)):
            if idx not in bad_trees_idx:
                newlist.append( reftrees[idx] )
        reftrees = newlist

        save_sequences(args.hyp + '.bad-trees', bad_trees)
        save_sequences(args.hyp + '.bad-gafl-trees', bad_oldtrees)
        save_sequences(args.hyp + '.bad-sources', bad_sources)
        save_sequences(args.hyp + '.bad-refs', bad_ref)

    assert len(reftrees) == len(hyptrees)

    print(' - Read {} trees from reference and hypothesis files...'.format(len(reftrees)))
    sys.stdout.flush()

    fr = open(args.ref + '.clean', 'w', encoding='utf-8')
    fh = open(args.hyp + '.clean', 'w', encoding='utf-8')

    difflens = 0
    difflvs = 0
    for i in range(len(reftrees)):
        reflvs = reftrees[i].leaves()
        hyplvs = hyptrees[i].leaves()

        if len(reflvs) != len(hyplvs):
            difflens += 1

            if len(reflvs) > len(hyplvs): 
                add_leaves = reflvs[len(hyplvs):]
                hyptrees[i] = add_subtree( hyptrees[i], add_leaves )
                hyplvs = hyptrees[i].leaves()
            else:
                add_leaves = hyplvs[len(reflvs):]
                reftrees[i] = add_subtree( reftrees[i], add_leaves )
                reflvs = reftrees[i].leaves()
 
            assert len(reflvs) == len(hyplvs)
            replace_leaves(hyptrees[i], reflvs)

            reflvs = reftrees[i].leaves()
            hyplvs = hyptrees[i].leaves()
            assert len(reflvs) == len(hyplvs)
            for j in range(len(reflvs)):
                assert reflvs[j] == hyplvs[j]
        else:
            difflvs += 1

            replace_leaves(hyptrees[i], reflvs)
            reflvs = reftrees[i].leaves()
            hyplvs = hyptrees[i].leaves()
            for j in range(len(reflvs)):
                assert reflvs[j] == hyplvs[j] 

        print(reftrees[i], file=fr)
        print('', file=fr)
        print(hyptrees[i], file=fh)
        print('', file=fh)

    fr.close()
    fh.close()

    print(' - Found {} trees with different leaves length'.format(difflens))
    print(' - Found {} trees with different leaves tokens'.format(difflvs))

if __name__ == '__main__':
    main()
